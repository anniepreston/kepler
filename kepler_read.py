#given CSV file from NASA exoplanet archive,
#create CSV file with converted coordinates for use in JS visualization program

# astropy: for coordinate conversion
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.coordinates import Distance

# for reading csv file
import csv

file = 'planets.csv'
write_file = 'converted_planets.csv'

with open(file, 'r') as csvfile:
    with open(write_file, 'w') as w_csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(w_csvfile, delimiter=',')
        writer.writerow(['x', 'y', 'z', 'temp', 'mass', 'date', 'distance', 'luminosity', 'mag', 'spectral'])
    
        for row in reader:
        #want: distance, RA, dec 
        #      temp, mass, radius (optional)
        #      also, potentially: orbital period, stellar mass
            if len(row) > 10 and row[0] != 'rowid' and row[42] != '':
                ra_deg = float(row[39]) # RA
                dec_deg = float(row[41]) # dec
                dist_pc = float(row[42]) # dist (pc)
                temp = row[51] # temp
                mass = row[21] # jupiter masses
                date = row[66] # date of last update
                distance = row[9] #semi-major axis (AU)
                luminosity = row[202] #log solar luminosity
                mag = row[46] #V-band optical magnitude
                spectral = row[193] #spectral type in letter/numeral format
            
                coords = SkyCoord(ra=ra_deg*u.degree, dec=dec_deg*u.degree, distance=dist_pc*u.pc)
            
                #write to file
                writer.writerow([coords.cartesian.x.value, coords.cartesian.y.value, coords.cartesian.z.value, temp, mass, date, distance, luminosity, mag, spectral])