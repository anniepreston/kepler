var svg = d3.select("body").append("svg")
	.attr("width", window.innerWidth)
	.attr("height", window.innerHeight);//.append("svg");
     
var temp_data = ["> 8000K", "< 4500K"];
const planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune'];
var discoveries = {'methods': ['Eye'], 'missions': [], 'combined': ['Eye']};

const width = window.innerWidth;
const height = window.innerHeight;
const startDate = new Date('1750-01-01');
const endDate = new Date('2018-01-01')
var absoluteYear = 1750; //start year
var absoluteMonth = 1;

const distScale = d3.scaleLog()
	.domain([0.005, 33])
	.range([0.05*width, 0.95*width]);
const solarDistScale = d3.scaleLog()
	.domain([0.2, 33])
	.range([0.05*width, 0.95*width]);
const massScale = d3.scaleLog()
	.domain([0.000174, 29])
	.range([600, 0]); // not sure what this is mapped to yet
const tempScale = d3.scaleLinear()
	//.domain([2516, 8590]) 'true' range
	.domain([2516,8000])
	.range([0.05*height, 0.92*height]);

const tempColorScale = d3.scaleSequential(d3.interpolateYlOrRd)
	.domain([2516,8590]);

const radiusScale = d3.scaleLinear()
	.domain([0.025, 2.0])
	.range([3, 13]);

var firstRadiusMultiplier = 7;
var secondRadiusMultiplier = 1;

var firstCutoffYear = 1967;
var secondCutoffYear = 2007;

const solarLabelFade = d3.scaleLinear()
		.domain([startDate, endDate])
		.range([0.8, 0.0]);

var exo_data;
var force;

//put these in order for now, to make later algorithm easy
const imageData = {
	'Mercury': [
		{'year': 1974, 'mission': 'Mariner 10', 'image': 'mariner-10-mercury.svg'},
		{'year': 2008, 'mission': 'Messenger', 'image': 'mercury_2008_messenger.svg'}
	],
	'Venus': [
		{'year': 1994, 'mission': 'Magellan', 'image': 'Magellan_Venus_globe.svg'}
	],
	'Earth': [
		{'year': 1968, 'mission': 'Apollo 8', 'image': 'earthrise.svg'},
		{'year': 1972, 'mission': 'Apollo 17', 'image': 'blue_marble.svg'}
	],
	'Mars': [
		{'year': 1971, 'mission': 'Mariner 9', 'image': 'mariner_9.svg'},
		{'year': 1976, 'mission': 'Viking 1', 'image': 'mars_viking.svg'},
		{'year': 2001, 'mission': 'Mars Global Surveyor', 'image': 'mars_2001_global_surveyor.svg'}
	],
	'Jupiter': [
		{'year': 1979, 'mission': 'Voyager 1', 'image': 'jupiter_voyager.svg'},
		{'year': 2000, 'mission': 'Cassini', 'image': 'jupiter_cassini.svg'}
	],
	'Saturn': [
		{'year': 1980, 'mission': 'Voyager 1', 'image': 'saturnx.svg'}
	],
	'Uranus': [
		{'year': 1986, 'mission': 'Voyager 2', 'image': 'uranus_voyager_2_1986.svg'},
		{'year': 2006, 'mission': 'Hubble Space Telescope', 'image': 'uranus_hubble_space_telescope_2006.svg'}
	],
	'Neptune': [
		{'year': 1989, 'mission': 'Voyager 2', 'image': 'neptune_1989_voyager_2.svg'}
	]
}

//const tempColors = {Cold: "#3288bd", Warm: "#ffffbf", Hot: "#d53e4f"};
//const tempColors = {Cold: "#66c2a5", Warm: "#ffffbf", Hot: "#f46d43"};
const tempColors = {Cold: "#3288bd", Warm: "#ffffbf", Hot: "#d53e4f"};

formatDate = d3.timeFormat("%B %Y");

var formatDateString = d3.timeFormat("%Y-%m-%d");

var parseDate = d3.timeFormat("%Y-%m-%d").parse;

//var exoplanets_csv = "https://bitbucket.org/anniepreston/kepler/raw/115ccb9d90bab31e623738ab70dfa6879b1114c3/converted_planets.csv";
var exoplanets_csv = './phl_exoplanets_and_solar_system.csv';

//exoplanets
d3.csv(exoplanets_csv, function(error, data) {
	if (error) {
        	console.log(error);
	} else {
		console.log("data: ", data);
		exo_data = data.filter(function(d) { 
			var date = new Date(d.year + '-' + d.month + '-12');
			return date < startDate && parseFloat(d.distance) > 0 && parseFloat(d.star_temp) > 0; 
		}); //for now, just filter out non-good data
		for (var i = 0; i < exo_data.length; i++){
			var idx = discoveries['methods'].indexOf(exo_data[i].discovery);
			if (idx == -1) {
				discoveries['methods'].push(exo_data[i].discovery);
				discoveries['combined'].push(exo_data[i].discovery);
			}
		}
    	drawExoplanets(exo_data, startDate);
    	//start timer...
    	var times = absoluteYear;
    	var months = 1;
    	//var interval = setInterval(incrementYear, 30);
    	var firstInterval = setInterval(function(){
    		times += 1;
    		if (times == firstCutoffYear){
    			clearInterval(firstInterval);
    			var secondInterval = setInterval(function(){
					//months += 1;
					//if (months == 12) 
					times += 1;
					if (times == secondCutoffYear){
						clearInterval(secondInterval);
						absoluteYear = secondCutoffYear;
						times = secondCutoffYear;
						setTimeout(function(){
							transitionDistanceRange();
						}, 300);
						setTimeout(function(){
							transitionAxes();
							var thirdInterval = setInterval(function(){
								times += 1;
			    				if (times == 2018){
			    					//spread();
			    					clearInterval(thirdInterval);
			    				}
			    				incrementYear();
			    			}, 500);
						},7000);
					}
					incrementYear(); //should actually start incrementing months here
				}, 700);
    		}
    		incrementYear();
    	}, 30);
    	var months = 1;


	}
});

function drawExoplanets(exo_data, filterDate){
	exo_data = parseImages(exo_data, filterDate);
	var new_discoveries = parseMethods(exo_data, filterDate);
	//^adjust discovery methods list down based on # of new entries here

	var	exo = svg.selectAll(".exo");
	exo = exo.data(exo_data);

	//force layout...let's try it!
	force = d3.forceSimulation()
		.nodes(exo_data)
	  	.force('charge', d3.forceManyBody().strength(1))
	  	.force('collide', d3.forceCollide().radius(function(d){
	  		var r = radiusScale(parseFloat(d.radius)); 
	        if (isNaN(r)) return 5+4;
	        else return r+4;
	  	}).strength(1))
	  	//.force('center', d3.forceCenter((100, 100)));
	  	.force('x', d3.forceX().x(function(d) { return distScale(parseFloat(d.distance, 10)); }).strength(2))
	  	.force('y', d3.forceY().y(function(d) { return tempScale(parseFloat(d.star_temp, 10)); }).strength(2));

	exo.enter()
		.append("circle")
		.attr("class", function(d){
			if (d.name == '') return 'exo';
			else return 'exo solar';
		})
	    .attr("r", function(d) {
	        		var r = radiusScale(parseFloat(d.radius)); 
	        		if (absoluteYear < secondCutoffYear){
	        			if (isNaN(r)) return firstRadiusMultiplier*5; 
	        			else return firstRadiusMultiplier*r;		
	        		}
	        		else {
	        			if (isNaN(r)) return secondRadiusMultiplier*5;
	        			else return secondRadiusMultiplier*r;
	        		}
	        	})
	    .attr("fill", function(d) {
	        			if (d.name != '' && typeof(d.image) == 'undefined'){    
	                		return "url(#radial-gradient)";}
	                	else if (typeof(d.image) != 'undefined'){
	                		return 'none';
	                	}
	                	else return '#333333';
	        		})
	    .attr("stroke", function(d){
			if (d.zone_class == 'Hot' || d.zone_class == 'Warm' || d.zone_class == 'Cold'){
	        	return tempColors[d.zone_class];
	        }
	        else return 'none';
	    })
	    .attr("stroke-width", 1)
	    .attr("stroke-opacity", 0.3)
	    .style("filter", function(d){
	    	if (d.name == ''){
	    		if (radiusScale(parseFloat(d.radius)) < 3){
	    			return "url(#smallGlow)";
	    		}
	    		else return "url(#mediumGlow)";
	    	}
	    	else return 'none';
	    })
	    .attr('opacity', function(d){
	    	if (d.name != ''){
	    		return 0.5;
	    	}
	    	else return 0.3;
	    })
    	.attr("cx", function(d) { 
    		if (absoluteYear < secondCutoffYear){
    			var dist = solarDistScale(parseFloat(d.distance, 10)).toString(); return dist; 
    		}
    		else {
    			var dist = distScale(parseFloat(d.distance, 10)).toString(); return dist;
    		}
    	})
    	.attr("cy", function(d) { var t = tempScale(parseFloat(d.star_temp, 10)).toString(); return t; });

	var	images = svg.selectAll(".image");
	images = images.data(exo_data);
    	
    images.enter()
    	.append("svg:image")
		.attr("class", "image") 
		.attr("xlink:href", function(d){
			if (d.name != ''){
				return d.image;
			}
		})
	    .attr("width", function(d){
	    	var r = radiusScale(parseFloat(d.radius)); 
	    	if (d.name == 'Saturn') r = 2.3*r;
	    	if (absoluteYear <= secondCutoffYear){
	    		return 2*firstRadiusMultiplier*r+4;
	    	}
	    	else return 2*secondRadiusMultiplier*r+4;
	    })
	    .attr("height", function(d){
	    	var r = radiusScale(parseFloat(d.radius)); 
	    	if (d.name == 'Saturn') r = 2.3*r;
	    	if (absoluteYear <= secondCutoffYear){
	    		return 2*firstRadiusMultiplier*r+4;
	    	}
	    	else return 2*secondRadiusMultiplier*r+4;
	    }) 
    	.attr("x", function(d) { 
    		if (absoluteYear <= secondCutoffYear){
    			var dist = solarDistScale(parseFloat(d.distance, 10)).toString(); return dist; 
    		}
    		else {
    			var dist = distScale(parseFloat(d.distance, 10)).toString(); return dist;
    		}
    	})
    	.attr("y", function(d) { var t = tempScale(parseFloat(d.star_temp, 10)).toString(); return t; })
    	.attr("transform", function(d){
    		//NOTE: change if there are rings involved... :(
    		var diff;
    		var saturn_adjust;
    		if (absoluteYear <= secondCutoffYear){
    			diff = firstRadiusMultiplier*radiusScale(parseFloat(d.radius)) + 2
    			saturn_adjust = 10;
    		}
    		else {
    			diff = secondRadiusMultiplier*radiusScale(parseFloat(d.radius))
    			saturn_adjust = 2;
    		}
    		if (d.name == 'Saturn') diff = 2*diff + saturn_adjust;
    		return "translate(" + "-" + diff + ",-" + diff + ")";
    	})
    	.attr("opacity", 0)
		.transition()
		.duration(1000)
		.attr('opacity', 1)

    //also, draw the 'suns' for the enter() data!
    var suns = svg.selectAll(".sun");
    suns = suns.data(exo_data);

    suns.enter()
    	.append("circle")
    	.attr("class", "sun")
    	.attr("cx", -90)
    	.attr("cy", function(d){ var t = tempScale(parseFloat(d.star_temp, 10)).toString(); return t; })
    	.attr("fill", function(d){ var color = tempColorScale(parseFloat(d.star_temp, 10)); return color; })
    	.attr("r", 100)
    	.attr("opacity", 0.1);

	exo.exit().remove();
	images.exit().remove();
	suns.exit().remove();

	//first, need to check whether/how much to translate existing ones
	var discovery_height = 20; //?
	var y_offset = 42;
	var discovery = svg.selectAll('.discovery');
	/*
	discovery
		.attr("transform", "translate(0," + discovery_height*new_discoveries.length);
		*/
	//now, add new discoveries and/or highlight repeated ones:
	discovery = discovery.data(exo_data);
	discovery.enter()
		.append("text")
		.attr("class", "discovery")
		.attr("transform", function(d){
			var x = 0;
			var y = 0;
			if (typeof(d.mission) != 'undefined'){
				var idx = discoveries['missions'].indexOf(d.mission); //missions
				if (idx != -1) {
					x = 0.8*width;
					y = (idx) * discovery_height + y_offset;
				}
			}
			else {
				var idx = discoveries['methods'].indexOf(d.discovery); //tools
				if (idx != -1){
					x = 0.8*width - 20;
					y = (idx) * discovery_height + y_offset;
				}
			}
			return "translate(" + x + "," + y + ")";
		})
		//.append("text")
		.attr("fill", function(d){
			if (typeof(d.mission) != 'undefined'){
				return "#c9ddff";
			}
			else return "#e0e0e0";
		}) //should be dependent
		.attr("opacity", 0.8)
		.attr("text-anchor", function(d){
			if (typeof(d.mission) != 'undefined'){
				return 'start';
			}
			else return 'end';
		})
		.attr("font-family", "sans-serif")
		.text(function(d){
			if (typeof(d.mission) != 'undefined'){
				return d.mission;
			}
			else {
				return d.discovery;
			}
		})
    	.transition()
    	.duration(2000)      
    	.attr('opacity', 0.3); 

		discovery.exit().remove();
};

function parseImages(exo_data, date){
	var year = date.getFullYear();
	var imageList = {};
	for (var i = 0; i < planets.length; i++){
		var image_method = getNewImage(imageData[planets[i]], year);
		var name = planets[i];
		if (image_method != '') { 
			var path = './images/' + planets[i] + '/' + image_method.image;
			var mission = image_method.mission;
			const newEntry = {
				...exo_data[i],
				image: path,
				mission: mission
			}
			exo_data.push(newEntry);
		}
	}
	return exo_data;
}

function parseMethods(exo_data, date){
	var year = date.getFullYear();
	new_data = exo_data.filter(function(d){
		return d.year == year;
	})
	new_discoveries = [];
	for (var i = 0; i < new_data.length; i++){
		method = new_data[i].discovery;
		var idx = discoveries['methods'].indexOf(method);
		if (idx == -1){
			new_discoveries.push(method);
			discoveries['methods'].push(method);
			discoveries['combined'].push(method);
		}
	}
	for (var i = 0; i < planets.length; i++){
		new_images = imageData[planets[i]].find(mission => mission.year == year);
		if (typeof(new_images) != 'undefined'){
			if (discoveries['missions'].indexOf(new_images.mission) == -1){
				new_discoveries.push(new_images.mission);
				discoveries['missions'].push(new_images.mission);
				discoveries['combined'].push(new_images.mission);
			}
		}
	}
	return new_discoveries;
}

function getNewImage(data, year){
	var valid_data = data.filter(function(d) { 
		return d.year <= year; 
	});
	if (valid_data.length > 0){
		var image = valid_data[valid_data.length-1].image;
		var mission = valid_data[valid_data.length-1].mission;
		return {'image': image, 'mission': mission};
	}
	else return '';
}

function transitionAxes(){
	var yAxis = d3.axisLeft()
		.scale(tempScale)
		.ticks(5, "0.0f")
		.tickSize(0, 6);
	      
	svg.append("g")
	    .attr("class", "yaxis")
	    .attr("transform", "translate(52,0)")
	    .call(yAxis)
	    .selectAll("text")
	    .attr("transform", "translate(-12,0),rotate(270)");
		
	//y axis label
	svg.append("text")
		.attr("x", 0)
		.attr("y", 0)
		.attr("transform", "translate(28," + 0.5*height + "),rotate(-90)")
		.attr("text-anchor", "middle")
		.attr("font-family", "sans-serif")
		.attr("fill", "#6d6d6d")
		.attr("font-size", "15px")
		.attr("opacity", 0.4)
		.text("host star temperature (K)");

	var xAxis = d3.axisBottom()
	    .scale(distScale)
	    .ticks(10, ".2f")
	    .tickSize(6,0);
	//fade in x axis
	svg.append("g")
	    .attr("class", "xaxis")
	    .attr("transform", "translate(0," + (height-52) + ")")
	    .call(xAxis)
	//x axis label
	svg.append("text")
		.attr("x", width/2)
		.attr("y", height - 16)
		.attr("text-anchor", "middle")
		.attr("font-family", "sans-serif")
		.attr("fill", "#6d6d6d")
		.attr("font-size", "15px")
		.attr("opacity", 0.3)
		.text("distance from host star (AU)");	
}

function transitionDistanceRange(){
	//re-locate
	svg.selectAll(".exo")
		.transition()
		.duration(3000)
		.attr("r", function(d){
	        var r = radiusScale(parseFloat(d.radius)); 
	        if (isNaN(r)) return 5; 
	        else return secondRadiusMultiplier*r;	
		})
		.attr("cx", function(d){ 
    		var dist = solarDistScale(parseFloat(d.distance, 10)).toString(); return dist;
		})
		.transition()
		.duration(4000)
		.attr("cx", function(d){ 
    		var dist = distScale(parseFloat(d.distance, 10)).toString(); return dist;
		});

		
	var	images = svg.selectAll(".image");
		images
			.transition()
			.duration(400)
			.attr("opacity", 0.0);
	//images.remove();
	/*
	images
	    .attr("width", function(d){
	    	var r = radiusScale(parseFloat(d.radius)); 
	    	if (d.name == 'Saturn') r = 2.3*r;
	        return 2*radiusMultiplier*r+4;
	    })
	    .attr("height", function(d){
	    	var r = radiusScale(parseFloat(d.radius)); 
	    	if (d.name == 'Saturn') r = 2.3*r;
	        return 2*radiusMultiplier*r+4;
	    }) 
    	.attr("x", function(d) { var dist = distScale(parseFloat(d.distance, 10)).toString(); return dist; })
    	.attr("y", function(d) { var t = tempScale(parseFloat(d.star_temp, 10)).toString(); return t; })
    	.attr("transform", function(d){
    		//NOTE: change if there are rings involved
    		var diff = radiusMultiplier*radiusScale(parseFloat(d.radius)) + 2
    		if (d.name == 'Saturn') diff = 2*diff + 10;
    		return "translate(" + "-" + diff + ",-" + diff + ")";
    	})
    	*/
}

// scale function
var timeScale = d3.scaleTime()
  .domain([startDate, endDate])
  .range([0.2*width, 0.85*width])
  .clamp(true);
/*
var expandButton = svg.append("rect")
	.attr("width", 20)
	.attr("height", 20)
	.attr("fill", "white")
	.attr('opacity', 0.5)
	.attr("transform", "translate(" + (20 + timeScale.range()[1]) + ',12)')
	.on("click", spread)
	*/

function spread() {
   	svg.selectAll('.exo')
      	.attr('cx', 0)
      	.attr('cy', 0);

	force.on('tick', function(){
	   	svg.selectAll('.exo')
	      	.attr('transform', function(d){
	         	return 'translate(' + d.x + ',' + d.y + ')'; });
	});
}

var slider = svg.append("g")
  .attr("class", "slider");

slider.selectAll(".extent,.resize")
  .remove();

slider.select(".background")
  .attr("height", 30);
/*
var handle = slider.append("g")
  .attr("class", "handle")
  .attr("transform", "translate(" + timeScale.range()[0] + ",0)");

handle.append("path")
	.attr("transform", "translate(0," + 8 + ")")
	.attr("d", "M 0 0 V 10");
	*/
	
slider.append('text')
  .text(startDate.getFullYear())
  .attr("text-anchor", "middle")
  .attr("transform", "translate(" + 0.5*width + " ," + 28 + ")");

function updatePos(elem) {
  //var value = brush.extent()[0];
   	var xPos = d3.mouse(elem)[0];
	var value = timeScale.invert(xPos);

	d3.csv(exoplanets_csv, function(error, data) {
		if (error) {
	    		console.log(error);
		} else {
			exo_data = data.filter(function(d) { 
			var date = new Date(d.year + '-' + d.month + '-12');
			return date < value && parseFloat(d.distance) > 0 && parseFloat(d.star_temp) > 0; });
			drawExoplanets(exo_data, value);
	    }
    });
	var newXPos = xPos;
	if (xPos >= timeScale.range()[1]) newXPos = timeScale.range()[1];
	if (xPos <= timeScale.range()[0]) newXPos = timeScale.range()[0];

	handle.attr("transform", "translate(" + newXPos + ",0)");
	slider.select('text').text(value.getFullYear());  
	svg.selectAll(".solarLabel").attr("opacity", function(){ return solarLabelFade(value); });
}

function incrementYear(){
	absoluteYear += 1;
	d3.csv(exoplanets_csv, function(error, data) {
		if (error) {
			console.log(error);
		} else {
			exo_data = data.filter(function(d) {
				var date = d.year;
				return date <= absoluteYear && parseFloat(d.distance) > 0 && parseFloat(d.star_temp) > 0; 
			});
			drawExoplanets(exo_data, new Date(absoluteYear + '-12' + '-12'));
		}
	});
	slider.select('text').text(absoluteYear);  
}
function incrementMonth(){
	var currentDate = new Date(absoluteYear + '-' + String(absoluteMonth).padStart(2,'0') + '-12');
	d3.csv(exoplanets_csv, function(error, data){
		if (error) {
			console.log(error);
		} else {
			exo_data = data.filter(function(d) {
				var date = new Date(d.year + '-' + d.month + '-12');
				return date < currentDate && parseFloat(d.distance) > 0 && parseFloat(d.star_temp) > 0;
			});
			drawExoplanets(exo_data, currentDate);
		}
	});
	slider.select('text').text(absoluteYear);

	//that whole months/years thing
	absoluteMonth += 1;
	if (absoluteMonth == 13) { 
		absoluteMonth = 1;
		absoluteYear += 1;
	}
}
    
// glowy filters
//Container for the gradients
var defs = svg.append("defs");

//Code taken from http://stackoverflow.com/questions/9630008/how-can-i-create-a-glow-around-a-rectangle-with-svg
//Filter for the outside glow
var filter = defs.append("filter")
	.attr("id","smallGlow");

filter.append("feGaussianBlur")
	.attr("class", "blur")
	.attr("stdDeviation", 0.1)
	.attr("result","coloredBlur");

var feMerge = filter.append("feMerge");
feMerge.append("feMergeNode")
	.attr("in","coloredBlur");
feMerge.append("feMergeNode")
	.attr("in","SourceGraphic");

var mediumFilter = defs.append("filter")
	.attr("id", "mediumGlow");
mediumFilter.append("feGaussianBlur")
	.attr("class", "blur")
	.attr("stdDeviation", 0.3)
	.attr("result","coloredBlur");
var mediumFeMerge = mediumFilter.append("feMerge");
mediumFeMerge.append("feMergeNode")
	.attr("in","coloredBlur");
mediumFeMerge.append("feMergeNode")
	.attr("in","SourceGraphic");

var largeFilter = defs.append("filter")
	.attr("id", "largeGlow");

largeFilter.append("feGaussianBlur")
	.attr("class", "blur")
	.attr("stdDeviation", 5)
	.attr("result","coloredBlur");

var largeFeMerge = largeFilter.append("feMerge");
largeFeMerge.append("feMergeNode")
	.attr("in","coloredBlur");
largeFeMerge.append("feMergeNode")
	.attr("in","SourceGraphic");

//radial gradient for solar system planets
//(maybe also just do the glows this way to save effort)
var radialGradient = svg.append("defs")
  .append("radialGradient")
    .attr("id", "radial-gradient");

radialGradient.append("stop")
    .attr("offset", "0%")
    .attr("stop-color", "#e8e8e8")
    .attr("stop-opacity", 0.7);

radialGradient.append("stop")
    .attr("offset", "100%")
    .attr("stop-color", "##e8e8e8")
    .attr("stop-opacity", 0.0);
