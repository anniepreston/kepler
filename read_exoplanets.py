#given CSV file from * exoplanets.org * archive,
#create CSV file with converted coordinates for use in JS visualization program

# astropy: for coordinate conversion
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.coordinates import Distance

# for reading csv file
import csv

file = 'exoplanets.csv'
write_file = 'converted_exoplanets.csv'

with open(file, 'r') as csvfile:
    with open(write_file, 'w') as w_csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.writer(w_csvfile, delimiter=',')
        writer.writerow(['name', 'temp', 'star_temp', 'mass', 'date', 'distance', 'mag', 'discovery', 'radius'])
    
        i = 0
        for row in reader:
        #want: distance, RA, dec 
        #      temp, mass, radius (optional)
        #      also, potentially: orbital period, stellar mass
            """
            if i == 0:
                print(float(row['RA']))
            ra_deg = float(row['RA']) # RA
            dec_deg = float(row['DEC']) # dec
            if i == 0:
                print(float(row['DEC']))
            if i == 0:
                print(row['DIST'])
            dist_pc = float(row['DIST']) # dist (pc)
            """

            temp = '400' #row[51] # temp: FIXME
            star_temp = row['TEFF']
            if star_temp != '':
                star_temp = float(star_temp)
            mass = row['MASS'] # jupiter masses
            if mass != '':
                mass = float(mass)

            date = row['DATE'] # date of last update
            if date != '':
                date = int(date)

            distance = row['A'] #semi-major axis (AU)
            if distance != '':
                distance = float(distance)
            #luminosity = row[202] #log solar luminosity
            mag = row['V'] #V-band optical magnitude
            #spectral = row[193] #spectral type in letter/numeral format
            discovery = row['PLANETDISCMETH']
            radius = row['R'] #planetary radius (Jupiter radii)
            if radius != '':
                radius = float(radius)
            name = ''
        
           # coords = SkyCoord(ra=ra_deg*u.degree, dec=dec_deg*u.degree, distance=dist_pc*u.pc)
        
            #write to file
            if date != '':
                writer.writerow([name, temp, star_temp, mass, date, distance, mag, discovery, radius])