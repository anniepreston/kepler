# given CSV file from * PHL (planetary habitability laboratory) * archive,...
# create CSV file with data for my visualization 

import numpy as np
import csv
import math

file = 'phl_hec_all_confirmed.csv'
write_file = 'phl_exoplanets_and_solar_system.csv'

with open(file, 'r') as csvfile:
    with open(write_file, 'w') as w_csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        writer = csv.writer(w_csvfile, delimiter=',')
        writer.writerow(['name', 'temp', 'star_temp', 'mass', 'year', 'month', 'distance', 'mag', 'discovery', 'radius', 'mass_class', 'zone_class', 'confirmed'])
    
        #solar system: just do it manually, w/e, so small
        writer.writerow(['Mercury', '400', '5777.0', '0.000174', '1000', '01', '0.39', '', 'Eye', '0.0349', '', '', '1'])
        writer.writerow(['Venus', '400', '5777.0', '0.002564', '1000', '01', '0.723', '', 'Eye', '0.0866', '', '', '1'])
        writer.writerow(['Earth', '400', '5777.0', '0.003146', '1000', '01', '1.0', '', 'Eye', '0.0911', '', '', '1'])
        writer.writerow(['Mars', '400', '5777.0', '0.000338', '1000', '01', '1.52', '', 'Eye', '0.0485', '', '', '1'])
        writer.writerow(['Jupiter', '400', '5777.0', '1', '1000', '01', '5.2', '', 'Eye', '1', '', '', '1'])
        writer.writerow(['Saturn', '400', '5777.0', '0.29941', '1000', '01', '9.6', '', 'Eye', '0.8329', '', '', '1'])
        writer.writerow(['Uranus', '400', '5777.0', '0.045735', '1781', '01', '19.2', '', 'Telescope', '0.3628', '', '', '1'])
        writer.writerow(['Neptune', '400', '5777.0', '0.053953', '1846', '01', '30.1', '', 'Telescope', '0.3522', '', '', '1'])

        for row in reader:
            temp = row['P. Ts Mean (K)']
            if temp != '':
                temp = float(temp)

            star_temp = row['S. Teff (K)']
            if star_temp != '':
                star_temp = float(star_temp)

            mass = row['P. Mass (EU)'] # earth masses
            if mass != '':
                mass = float(mass)*(5.9742e+24)/(1.8987e+27) #convert to jupiter masses

            year = row['P. Disc. Year'] # date of discovery/publication
            if year.lstrip() != '' and year.lstrip() != 'TTV':
                year = math.floor(float(year.lstrip()))
            else:
                year = year.lstrip()

            month = str(np.random.randint(1,13)).zfill(2) #not real months, but
                # populating to diversify the times when new planets pop up within each year

            distance = row['P. Sem Major Axis (AU)'] #semi-major axis (AU)
            if distance != '':
                distance = float(distance)

            mag = row['S. Appar Mag'] # apparent magnitude from earth
            if mag != '':
                mag = float(mag)

            discovery = row['P. Disc. Method'] #string
                #NOTE: Planet discovery method --> 
                # rv = radial velocity, 
                # tran = transiting, 
                # ima = imaging, 
                # micro = micro lensing, 
                # pul = pulsar timing, 
                # ast = astrometry.

            radius = row['P. Radius (EU)'] #planetary radius (earth radii)
            if radius != '':
                radius = float(radius)*6378136/71492000 #convert to jupiter radii

            name = ''

            mass_class = row['P. Mass Class'] #string
            zone_class = row['P. Zone Class'] #string
            confirmed = int(row['P. Confirmed']) #(this field shouldn't be missing)
                
            #write to file
            if year != '' and year != 'TTV':
                writer.writerow([name, temp, star_temp, mass, year, month, distance, mag, discovery, radius, mass_class, zone_class, confirmed])